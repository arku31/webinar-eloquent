<?php
require('../config.php');

use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::schema()->create('Posts', function ($table) {
    $table->increments('id');
    $table->string('title');
    $table->text('content');
    $table->timestamps();
});
