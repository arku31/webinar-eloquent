<?php require "header.php"; ?>
<table class="table">
    <tr>
        <th>Заголовок</th>
        <th>Сообщение</th>
    </tr>
    <?php $posts = \Models\Post::latest()->take(20)->get(); ?>
    <?php foreach ($posts as $post) : ?>
    <tr>
        <td><?=$post->title?></td>
        <td><?=$post->content?></td>
    </tr>
    <?php endforeach; ?>

</table>